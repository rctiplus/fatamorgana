# Fatamorgana #

* [Description](#description)
* [Importing package](#importing)
* [Documentation](#documentation)
* [FAQ](#faq)
* [License](#license)

### Description ###

Fatamorgana is a FFMPEG wrapper written in GO, the code itself is a modified version from https://github.com/floostack/transcoder. 

### Requirement
1. FFMPEG
2. FFPROBE

as this library is wrap FFMPEG and FFPROBE, so to get this library function, we must install FFMPEG and FFPROBE first.
you can search on how to install FFMPEG and FFPROBE based on your OSes.

1. Linux (Centos)
   
   https://linuxize.com/post/how-to-install-ffmpeg-on-centos-7/
2. Linux (Debian)
   
   https://linuxize.com/post/how-to-install-ffmpeg-on-debian-9/
3. Linux (Ubuntu)
   
   https://linuxize.com/post/how-to-install-ffmpeg-on-ubuntu-18-04/
4. Windows
   
   http://blog.gregzaal.com/how-to-install-ffmpeg-on-windows/
5. Mac
   
   https://github.com/fluent-ffmpeg/node-fluent-ffmpeg/wiki/Installing-ffmpeg-on-Mac-OS-X
   

### Importing

Before importing fatamorgana into your code, first download fatamorgana using `go get` tool.

```bash
$ go get bitbucket.org/rctiplus/fatamorgana 
```

To use the fatamorgana, simply by adding the following import statement to your `.go` files.

```go
import "bitbucket.org/rctiplus/fatamorgana"
```

## Sample use

```go
// (watermarking video)
package main

import (
	"bitbucket.org/rctiplus/fatamorgana/ffmpeg"
	"fmt"
	"log"
)

func main() {
	watermarkImage := "rhot.png"
	overwrite := true
	filterComplex := `[1:v]scale=80:40 [ovrl],[0:v][ovrl]overlay=main_w-overlay_w-5:main_h-overlay_h-8,drawtext=fontfile=OpenSans-Bold.ttf:text='@dimas_yudha_p':x=320:y=170: fontcolor=white:fontsize=8`

	opts := ffmpeg.Options{
		WatermarkImage: &watermarkImage,
		Overwrite:    &overwrite,
		FilterComplex: &filterComplex,
	}

	ffmpegConf := &ffmpeg.Config{
		FFMpegBinPath:   "/usr/local/bin/ffmpeg",
		FFProbeBinPath:  "/usr/local/bin/ffprobe",
		ProgressEnabled: true,
		Verbose: false,
	}

	metadata, err := ffmpeg.New(ffmpegConf).Input("sample.mov").GetMetadata()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(metadata.GetFormat().GetDuration())

	progress, err := ffmpeg.
		New(ffmpegConf).
		Input("sample.mov").
		Output("sample_watermark.mov").
		WithOptions(opts).
		Start(opts)

	if err != nil {
		log.Fatal(err)
	}

	for msg := range progress {
		log.Printf("%+v", msg)
	}
}
```

## Documentation

Further documentation can be found on [pkg.go.dev](https://pkg.go.dev/bitbucket.org/rctiplus/fatamorgana)

## FAQ

**Can I contribute to make Fatamorgana?**

[Please do!](https://bitbucket.org/rctiplus/gollum/blob/master/CONTRIBUTING.md) We are looking for any kind of contribution to improve fatamorgana core funtionality and documentation. When in doubt, make a PR!


## License

```
Copyright (c) 2021, RCTIPlus

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```