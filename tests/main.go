package main

import (
	"bitbucket.org/rctiplus/fatamorgana/ffmpeg"
	"fmt"
	"log"
)

func main() {
	watermarkImage := "rhot.png"
	overwrite := true
	filterComplex := `[1:v]scale=80:40 [ovrl],[0:v][ovrl]overlay=main_w-overlay_w-5:main_h-overlay_h-8,drawtext=fontfile=OpenSans-Bold.ttf:text='@dimas_yudha_p':x=320:y=170: fontcolor=white:fontsize=8`

	opts := ffmpeg.Options{
		WatermarkImage: &watermarkImage,
		Overwrite:    &overwrite,
		FilterComplex: &filterComplex,
	}

	ffmpegConf := &ffmpeg.Config{
		FfmpegBinPath:   "/usr/local/bin/ffmpeg",
		FfprobeBinPath:  "/usr/local/bin/ffprobe",
		ProgressEnabled: true,
		Verbose: false,
	}

	metadata, err := ffmpeg.New(ffmpegConf).Input("sample.mov").GetMetadata()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(metadata.GetFormat().GetDuration())

	progress, err := ffmpeg.
		New(ffmpegConf).
		Input("sample.mov").
		Output("sample_watermark.mov").
		WithOptions(opts).
		Start(opts)

	if err != nil {
		log.Fatal(err)
	}

	for msg := range progress {
		log.Printf("%+v", msg)
	}
}
