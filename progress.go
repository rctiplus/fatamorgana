package fatamorgana

// Progress abstraction functions
type Progress interface {
	GetFramesProcessed() string
	GetCurrentTime() string
	GetCurrentBitrate() string
	GetProgress() float64
	GetSpeed() string
}
