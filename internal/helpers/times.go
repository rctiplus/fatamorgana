package helpers

import (
	"bitbucket.org/rctiplus/almasbub"
	"strings"
)

// DurToSec convert duration to second
func DurToSec(dur string) (sec float64) {
	durAry := strings.Split(dur, ":")
	var secs float64
	if len(durAry) != 3 {
		return
	}
	hr := almasbub.ToFloat64(durAry[0])
	secs = hr * (60 * 60)
	min := almasbub.ToFloat64(durAry[1])
	secs += min * (60)
	second := almasbub.ToFloat64(durAry[2])
	secs += second
	return secs
}
