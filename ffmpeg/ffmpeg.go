package ffmpeg

import (
	"bitbucket.org/rctiplus/almasbub"
	"bitbucket.org/rctiplus/fatamorgana"
	"bitbucket.org/rctiplus/fatamorgana/internal/helpers"
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

// Transcoder model
type Transcoder struct {
	config           *Config
	input            string
	output           []string
	options          [][]string
	metadata         fatamorgana.Metadata
	inputPipeReader  *io.ReadCloser
	outputPipeReader *io.ReadCloser
	inputPipeWriter  *io.WriteCloser
	outputPipeWriter *io.WriteCloser
}

// New instance of transcoder with config
func New(cfg *Config) fatamorgana.Transcoder {
	return &Transcoder{config: cfg}
}

// Start transcoding process
func (t *Transcoder) Start(opts fatamorgana.Options) (<-chan fatamorgana.Progress, error) {

	var stderrIn io.ReadCloser

	out := make(chan fatamorgana.Progress)
	defer t.closePipes()

	// Validates config
	if err := t.validate(); err != nil {
		return nil, err
	}

	// Get file metadata
	_, err := t.GetMetadata()
	if err != nil {
		return nil, err
	}

	// Append input file and standard options
	args := append([]string{"-i", t.input}, opts.GetStrArguments()...)
	outputLength := len(t.output)
	optionsLength := len(t.options)

	fmt.Println("arguments:",args)
	if outputLength == 1 && optionsLength == 0 {
		// Just append the 1 output file we've got
		args = append(args, t.output[0])
	} else {
		for index, out := range t.output {
			// Get executable flags
			// If we are at the last output file but still have several options, append them all at once
			if index == outputLength-1 && outputLength < optionsLength {
				for i := index; i < len(t.options); i++ {
					args = append(args, t.options[i]...)
				}
				// Otherwise just append the current options
			} else {
				args = append(args, t.options[index]...)
			}

			// Append output flag
			args = append(args, out)
		}
	}

	// Initialize command
	cmd := exec.Command(t.config.FFMpegBinPath, args...)
	//cmd := exec.Command("/usr/local/bin/ffmpeg", strings.Split(`-i sample.mov -i rhot.png -y -filter_complex "[1:v]scale=80:40 [ovrl],[0:v][ovrl]overlay=main_w-overlay_w-5:main_h-overlay_h-8,drawtext=fontfile=OpenSans-Bold.ttf:text='@dimas_yudha_p':x=320:y=170: fontcolor=white:fontsize=8" watermark.mov`, " "))
	//cmd := exec.Command(`/usr/local/bin/ffmpeg`)
	fmt.Println(cmd.String())

	// If progresss enabled, get stderr pipe and start progress process
	if t.config.ProgressEnabled && !t.config.Verbose {
		stderrIn, err = cmd.StderrPipe()
		if err != nil {
			return nil, fmt.Errorf("failed getting transcoding progress (%s) with args (%s) with error %s", t.config.FFMpegBinPath, args, err)
		}
	}

	if t.config.Verbose {
		cmd.Stderr = os.Stdout
	}

	// Start process
	err = cmd.Start()
	if err != nil {
		return nil, fmt.Errorf("failed starting transcoding (%s) with args (%s) with error %s", t.config.FFMpegBinPath, args, err)
	}

	if t.config.ProgressEnabled && !t.config.Verbose {
		go func() {
			t.progress(stderrIn, out)
		}()

		go func() {
			defer close(out)
			err = cmd.Wait()
		}()
	} else {
		err = cmd.Wait()
	}

	return out, nil
}

// Input set input arguments
func (t *Transcoder) Input(arg string) fatamorgana.Transcoder {
	t.input = arg
	return t
}

// Output set output arguments
func (t *Transcoder) Output(arg string) fatamorgana.Transcoder {
	t.output = append(t.output, arg)
	return t
}

// InputPipe set inputpipe data transfer from
func (t *Transcoder) InputPipe(w *io.WriteCloser, r *io.ReadCloser) fatamorgana.Transcoder {
	if &t.input == nil {
		t.inputPipeWriter = w
		t.inputPipeReader = r
	}
	return t
}

// OutputPipe set outputpipe data transfer to
func (t *Transcoder) OutputPipe(w *io.WriteCloser, r *io.ReadCloser) fatamorgana.Transcoder {
	if &t.output == nil {
		t.outputPipeWriter = w
		t.outputPipeReader = r
	}
	return t
}

// WithOptions Sets the options object
func (t *Transcoder) WithOptions(opts fatamorgana.Options) fatamorgana.Transcoder {
	t.options = [][]string{opts.GetStrArguments()}
	return t
}

// WithAdditionalOptions Appends an additional options object
func (t *Transcoder) WithAdditionalOptions(opts fatamorgana.Options) fatamorgana.Transcoder {
	t.options = append(t.options, opts.GetStrArguments())
	return t
}

// validate transcoder
func (t *Transcoder) validate() error {
	if t.config.FFMpegBinPath == "" {
		return errors.New("ffmpeg binary path not found")
	}

	if t.input == "" {
		return errors.New("missing input option")
	}

	outputLength := len(t.output)

	if outputLength == 0 {
		return errors.New("missing output option")
	}

	// length of output files being greater than length of options would produce an invalid ffmpeg command
	// unless there is only 1 output file, which obviously wouldn't be a problem
	if outputLength > len(t.options) && outputLength != 1 {
		return errors.New("number of options and output files does not match")
	}

	for index, output := range t.output {
		if output == "" {
			return fmt.Errorf("output at index %d is an empty string", index)
		}
	}

	return nil
}

// GetMetadata Returns metadata for the specified input file
func (t *Transcoder) GetMetadata() (fatamorgana.Metadata, error) {

	if t.config.FFProbeBinPath != "" {
		var outb, errb bytes.Buffer

		input := t.input
		if t.inputPipeReader != nil {
			input = "pipe:"
		}

		args := []string{"-i", input, "-print_format", "json", "-show_format", "-show_streams", "-show_error"}
		cmd := exec.Command(t.config.FFProbeBinPath, args...)
		cmd.Stdout = &outb
		cmd.Stderr = &errb

		err := cmd.Run()
		if err != nil {
			return nil, fmt.Errorf("error executing (%s) with args (%s) | error: %s | message: %s %s", t.config.FFProbeBinPath, args, err, outb.String(), errb.String())
		}

		var metadata Metadata

		if err = json.Unmarshal([]byte(outb.String()), &metadata); err != nil {
			return nil, err
		}

		t.metadata = metadata

		return metadata, nil
	}

	return nil, errors.New("ffprobe binary not found")
}

// progress sends through given channel the transcoding status
func (t *Transcoder) progress(stream io.ReadCloser, out chan fatamorgana.Progress) {

	defer func() {
		err := stream.Close()
		if err != nil {
			log.Println("failed closed stream, error : ", err)
		}
	}()

	split := func(data []byte, atEOF bool) (advance int, token []byte, splitErr error) {
		if atEOF && len(data) == 0 {
			return 0, nil, nil
		}
		if i := bytes.IndexByte(data, '\n'); i >= 0 {
			// We have a full newline-terminated line.
			return i + 1, data[0:i], nil
		}
		if i := bytes.IndexByte(data, '\r'); i >= 0 {
			// We have a cr terminated line
			return i + 1, data[0:i], nil
		}
		if atEOF {
			return len(data), data, nil
		}

		return 0, nil, nil
	}

	scanner := bufio.NewScanner(stream)
	scanner.Split(split)

	buf := make([]byte, 2)
	scanner.Buffer(buf, bufio.MaxScanTokenSize)

	for scanner.Scan() {
		Progress := new(Progress)
		line := scanner.Text()

		if strings.Contains(line, "frame=") && strings.Contains(line, "time=") && strings.Contains(line, "bitrate=") {
			var re = regexp.MustCompile(`=\s+`)
			st := re.ReplaceAllString(line, `=`)

			f := strings.Fields(st)

			var framesProcessed string
			var currentTime string
			var currentBitrate string
			var currentSpeed string

			for j := 0; j < len(f); j++ {
				field := f[j]
				fieldSplit := strings.Split(field, "=")

				if len(fieldSplit) > 1 {
					fieldName := strings.Split(field, "=")[0]
					fieldValue := strings.Split(field, "=")[1]

					if fieldName == "frame" {
						framesProcessed = fieldValue
					}

					if fieldName == "time" {
						currentTime = fieldValue
					}

					if fieldName == "bitrate" {
						currentBitrate = fieldValue
					}
					if fieldName == "speed" {
						currentSpeed = fieldValue
					}
				}
			}

			timeSec := helpers.DurToSec(currentTime)
			durationSec := almasbub.ToFloat64(t.metadata.GetFormat().GetDuration())

			progress := (timeSec * 100) / durationSec
			Progress.Progress = progress

			Progress.CurrentBitrate = currentBitrate
			Progress.FramesProcessed = framesProcessed
			Progress.CurrentTime = currentTime
			Progress.Speed = currentSpeed

			out <- *Progress
		}
	}
}

// closePipes Closes pipes if opened
func (t *Transcoder) closePipes() {
	if t.inputPipeReader != nil {
		ipr := *t.inputPipeReader
		err := ipr.Close()
		if err != nil {
			log.Println("failed close input pipes, error : ", err)
		}
	}

	if t.outputPipeWriter != nil {
		opr := *t.outputPipeWriter
		err := opr.Close()
		if err != nil {
			log.Println("failed close output pipes, error : ", err)
		}
	}
}
