package ffmpeg

import "bitbucket.org/rctiplus/fatamorgana"

// Metadata data model
type Metadata struct {
	Format  Format    `json:"format"`
	Streams []Streams `json:"streams"`
}

// Format data model
type Format struct {
	Filename       string
	NbStreams      int    `json:"nb_streams"`
	NbPrograms     int    `json:"nb_programs"`
	FormatName     string `json:"format_name"`
	FormatLongName string `json:"format_long_name"`
	Duration       string `json:"duration"`
	Size           string `json:"size"`
	BitRate        string `json:"bit_rate"`
	ProbeScore     int    `json:"probe_score"`
	Tags           Tags   `json:"tags"`
}

// Streams data model
type Streams struct {
	Index              int
	ID                 string      `json:"id"`
	CodecName          string      `json:"codec_name"`
	CodecLongName      string      `json:"codec_long_name"`
	Profile            string      `json:"profile"`
	CodecType          string      `json:"codec_type"`
	CodecTimeBase      string      `json:"codec_time_base"`
	CodecTagString     string      `json:"codec_tag_string"`
	CodecTag           string      `json:"codec_tag"`
	Width              int         `json:"width"`
	Height             int         `json:"height"`
	CodedWidth         int         `json:"coded_width"`
	CodedHeight        int         `json:"coded_height"`
	HasBFrames         int         `json:"has_b_frames"`
	SampleAspectRatio  string      `json:"sample_aspect_ratio"`
	DisplayAspectRatio string      `json:"display_aspect_ratio"`
	PixFmt             string      `json:"pix_fmt"`
	Level              int         `json:"level"`
	ChromaLocation     string      `json:"chroma_location"`
	Refs               int         `json:"refs"`
	QuarterSample      string      `json:"quarter_sample"`
	DivxPacked         string      `json:"divx_packed"`
	RFrameRrate        string      `json:"r_frame_rate"`
	AvgFrameRate       string      `json:"avg_frame_rate"`
	TimeBase           string      `json:"time_base"`
	DurationTs         int         `json:"duration_ts"`
	Duration           string      `json:"duration"`
	Disposition        Disposition `json:"disposition"`
	BitRate            string      `json:"bit_rate"`
}

// Tags data model
type Tags struct {
	Encoder string `json:"ENCODER"`
}

// Disposition data model
type Disposition struct {
	Default         int `json:"default"`
	Dub             int `json:"dub"`
	Original        int `json:"original"`
	Comment         int `json:"comment"`
	Lyrics          int `json:"lyrics"`
	Karaoke         int `json:"karaoke"`
	Forced          int `json:"forced"`
	HearingImpaired int `json:"hearing_impaired"`
	VisualImpaired  int `json:"visual_impaired"`
	CleanEffects    int `json:"clean_effects"`
}

// GetFormat transcoder
func (m Metadata) GetFormat() fatamorgana.Format {
	return m.Format
}

// GetStreams transcoder
func (m Metadata) GetStreams() (streams []fatamorgana.Streams) {
	for _, element := range m.Streams {
		streams = append(streams, element)
	}
	return streams
}

// GetFilename transcoder
func (f Format) GetFilename() string {
	return f.Filename
}

// GetNbStreams transcoder
func (f Format) GetNbStreams() int {
	return f.NbStreams
}

// GetNbPrograms transcoder
func (f Format) GetNbPrograms() int {
	return f.NbPrograms
}

// GetFormatName transcoder
func (f Format) GetFormatName() string {
	return f.FormatName
}

// GetFormatLongName transcoder
func (f Format) GetFormatLongName() string {
	return f.FormatLongName
}

// GetDuration transcoder
func (f Format) GetDuration() string {
	return f.Duration
}

// GetSize transcoder
func (f Format) GetSize() string {
	return f.Size
}

// GetBitRate transcoder
func (f Format) GetBitRate() string {
	return f.BitRate
}

// GetProbeScore transcoder
func (f Format) GetProbeScore() int {
	return f.ProbeScore
}

// GetTags transcoder
func (f Format) GetTags() fatamorgana.Tags {
	return f.Tags
}

// GetEncoder transcoder
func (t Tags) GetEncoder() string {
	return t.Encoder
}

// GetIndex transcoder
func (s Streams) GetIndex() int {
	return s.Index
}

// GetID transcoder
func (s Streams) GetID() string {
	return s.ID
}

// GetCodecName transcoder
func (s Streams) GetCodecName() string {
	return s.CodecName
}

// GetCodecLongName transcoder
func (s Streams) GetCodecLongName() string {
	return s.CodecLongName
}

// GetProfile transcoder
func (s Streams) GetProfile() string {
	return s.Profile
}

// GetCodecType transcoder
func (s Streams) GetCodecType() string {
	return s.CodecType
}

// GetCodecTimeBase transcoder
func (s Streams) GetCodecTimeBase() string {
	return s.CodecTimeBase
}

// GetCodecTagString transcoder
func (s Streams) GetCodecTagString() string {
	return s.CodecTagString
}

// GetCodecTag transcoder
func (s Streams) GetCodecTag() string {
	return s.CodecTag
}

// GetWidth transcoder
func (s Streams) GetWidth() int {
	return s.Width
}

// GetHeight transcoder
func (s Streams) GetHeight() int {
	return s.Height
}

// GetCodedWidth transcoder
func (s Streams) GetCodedWidth() int {
	return s.CodedWidth
}

// GetCodedHeight transcoder
func (s Streams) GetCodedHeight() int {
	return s.CodedHeight
}

// GetHasBFrames transcoder
func (s Streams) GetHasBFrames() int {
	return s.HasBFrames
}

// GetSampleAspectRatio transcoder
func (s Streams) GetSampleAspectRatio() string {
	return s.SampleAspectRatio
}

// GetDisplayAspectRatio transcoder
func (s Streams) GetDisplayAspectRatio() string {
	return s.DisplayAspectRatio
}

// GetPixFmt transcoder
func (s Streams) GetPixFmt() string {
	return s.PixFmt
}

// GetLevel transcoder
func (s Streams) GetLevel() int {
	return s.Level
}

// GetChromaLocation transcoder
func (s Streams) GetChromaLocation() string {
	return s.ChromaLocation
}

// GetRefs transcoder
func (s Streams) GetRefs() int {
	return s.Refs
}

// GetQuarterSample transcoder
func (s Streams) GetQuarterSample() string {
	return s.QuarterSample
}

// GetDivxPacked transcoder
func (s Streams) GetDivxPacked() string {
	return s.DivxPacked
}

// GetRFrameRrate transcoder
func (s Streams) GetRFrameRrate() string {
	return s.RFrameRrate
}

// GetAvgFrameRate transcoder
func (s Streams) GetAvgFrameRate() string {
	return s.AvgFrameRate
}

// GetTimeBase transcoder
func (s Streams) GetTimeBase() string {
	return s.TimeBase
}

// GetDurationTs transcoder
func (s Streams) GetDurationTs() int {
	return s.DurationTs
}

// GetDuration transcoder
func (s Streams) GetDuration() string {
	return s.Duration
}

// GetDisposition transcoder
func (s Streams) GetDisposition() fatamorgana.Disposition {
	return s.Disposition
}

// GetBitRate transcoder
func (s Streams) GetBitRate() string {
	return s.BitRate
}

// GetDefault transcoder
func (d Disposition) GetDefault() int {
	return d.Default
}

// GetDub transcoder
func (d Disposition) GetDub() int {
	return d.Dub
}

// GetOriginal transcoder
func (d Disposition) GetOriginal() int {
	return d.Original
}

// GetComment transcoder
func (d Disposition) GetComment() int {
	return d.Comment
}

// GetLyrics transcoder
func (d Disposition) GetLyrics() int {
	return d.Lyrics
}

// GetKaraoke transcoder
func (d Disposition) GetKaraoke() int {
	return d.Karaoke
}

//GetForced ...
func (d Disposition) GetForced() int {
	return d.Forced
}

// GetHearingImpaired transcoder
func (d Disposition) GetHearingImpaired() int {
	return d.HearingImpaired
}

// GetVisualImpaired transcoder
func (d Disposition) GetVisualImpaired() int {
	return d.VisualImpaired
}

// GetCleanEffects transcoder
func (d Disposition) GetCleanEffects() int {
	return d.CleanEffects
}
