package ffmpeg

// Config transcoder data model
type Config struct {
	FFMpegBinPath   string
	FFProbeBinPath  string
	ProgressEnabled bool
	Verbose         bool
}
