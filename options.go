package fatamorgana

// Options abstraction functions
type Options interface {
	GetStrArguments() []string
}

